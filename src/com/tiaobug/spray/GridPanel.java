package com.tiaobug.spray;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import com.tiaobug.spray.domain.GridTabelModel;

public class GridPanel extends JPanel {

	private static final long serialVersionUID = 7070107229808209134L;

	public GridPanel() throws Exception{
		intiGridPanel();
	}

	public void intiGridPanel() {
		table = new JTable(data, columnNames);
		table.setModel(tableModel);// 禁止编辑
		table.setPreferredScrollableViewportSize(new Dimension(638, 280));
		table.setForeground(new Color(0, 0, 0));
		table.setFont(getFont().deriveFont(Font.BOLD));
		table.setSelectionBackground(Color.GRAY);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getTableHeader().setPreferredSize(new Dimension(table.getTableHeader().getWidth(), 32));// 表头高度
		table.setRowHeight(22);// 表格高度
		table.getTableHeader().setReorderingAllowed(false);// 表头不可拖动
		SetColumnWidth(table);
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane);
	}
	

	
	public static final String[] columnNames = { "编号", "管径(mm)", "喷头数(个)", "管道长度(m)","流量(L/s)", "流速(m/s)", "水损系数(KPa/m)", "沿程水损(m)" };
	private Object[][] data = new Object[0][8];
	
	private int readElementCount;
	private int columnCount;
	private JTable table;
	private DefaultTableModel tableModel = new GridTabelModel(data, columnNames); 
	

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	//自动行宽
	public void SetColumnWidth(JTable table) {
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); // 关闭列宽自动调整，当列宽超过窗口宽度时，显示水平滚动栏
		readElementCount = 0;
		columnCount = table.getColumnCount();
		JTableHeader header = table.getTableHeader();
		int rowCount = table.getRowCount();
		Enumeration<TableColumn> columns = table.getColumnModel().getColumns();
		while (columns.hasMoreElements()) {
			readElementCount++;
			TableColumn column = (TableColumn) columns.nextElement();
			int col = header.getColumnModel().getColumnIndex(column.getIdentifier());
			int width = (int) table.getTableHeader().getDefaultRenderer().getTableCellRendererComponent(table, column.getIdentifier(), false, false, -1, col).getPreferredSize().getWidth();
			for (int row = 0; row < rowCount; row++) {
				int preferedWidth = (int) table.getCellRenderer(row, col).getTableCellRendererComponent(table, table.getValueAt(row, col), false, false, row, col).getPreferredSize().getWidth();
				width = Math.max(width, preferedWidth);
			}
			header.setResizingColumn(column);
			column.setWidth(width + table.getIntercellSpacing().width+21);
		}
	}

	public boolean hasMoreElements() {
		return readElementCount < columnCount;
	}

	public Object nextElement() {
		return table.getColumnModel().getColumn(readElementCount - 1);
	}


}
