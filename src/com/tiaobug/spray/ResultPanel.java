package com.tiaobug.spray;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.tiaobug.spray.listeners.ButtonActionHandler;
import com.tiaobug.spray.listeners.HelpActionListener;
import com.tiaobug.spray.util.PropertiesUtil;

public class ResultPanel extends JPanel {

	private static final long serialVersionUID = -847268762782661490L;

	public ResultPanel() {
		super();
		intiGridPanel();
	}

	public void intiGridPanel() {
		localLossRate=new JComboBox(PropertiesUtil.getPropertiesArray("localLoss"));
		lossAlongWay=new JTextField();
		lossAlongWay.setEditable(false);
		lossAll=new JTextField();
		lossAll.setEditable(false);
		calButton=new JButton(ButtonActionHandler.CALCULATE);
		calButton.addActionListener(new ButtonActionHandler());
		exportButton=new JButton(ButtonActionHandler.EXPORT);
		exportButton.addActionListener(new ButtonActionHandler());

				
		JPanel buttonPane = new JPanel();
		buttonPane.setPreferredSize(new Dimension(680, 60));
		buttonPane.setLayout(new GridLayout(0,6,4,8));
		buttonPane.setBorder(BorderFactory.createTitledBorder("分组框")); //设置面板边框，实现分组框的效果，此句代码为关键代码
		 
		buttonPane.setBorder(BorderFactory.createLineBorder(Color.WHITE));//设置面板边框颜色

		JLabel localLossJLabel = new JLabel("局部损失比例：");// 管径类型
		JLabel lossAlongWayJLabel = new JLabel("沿程损失：");// 喷头数目
		JLabel lossAllLabel = new JLabel("总损失：");// 管道长度
		localLossJLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lossAlongWayJLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lossAllLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JButton helpImage=new JButton(new ImageIcon( System.getProperty("user.dir")+"/sources/help.png"));
		helpImage.setHorizontalAlignment(SwingConstants.LEFT);
		helpImage.setOpaque(false);
		helpImage.setBorder(null);
		helpImage.setContentAreaFilled(false);
		helpImage.setBounds(2, 2, 3, 3);
		helpImage.addActionListener(new HelpActionListener());

		buttonPane.add(lossAlongWayJLabel);
		buttonPane.add(lossAlongWay);
		buttonPane.add(localLossJLabel);
		buttonPane.add(localLossRate);
		buttonPane.add(lossAllLabel);
		buttonPane.add(lossAll);
		
		buttonPane.add(helpImage);

		buttonPane.add(new JLabel());
		buttonPane.add(new JLabel());
		buttonPane.add(new JLabel());
		
		
		buttonPane.add(calButton);
		buttonPane.add(exportButton);
		add(buttonPane);
	}
	
	
	


	public JComboBox getLocalLossRate() {
		return localLossRate;
	}

	public JTextField getLossAlongWay() {
		return lossAlongWay;
	}

	public JTextField getLossAll() {
		return lossAll;
	}





	private JComboBox localLossRate;//局部损失比例选择
	private JTextField lossAlongWay;//总的沿程水损
	private JTextField lossAll;//总水损
	private JButton calButton;//计算总额
	private JButton exportButton;//导出记事本
	
	


}
