package com.tiaobug.spray;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import com.tiaobug.spray.util.PropertiesUtil;

public class MainUI extends JFrame {

	private static final long serialVersionUID = 7370136874933105297L;

	/**
	 * @author tiaobug
	 * @since 2013-3-23 22:44:24
	 * @see www.tiaobug.com <h1>喷淋计算器</h1> <h3>为我的好基友老张兄而开发，帮助其减轻计算工作。</h3>
	 *      <p>
	 *      计算喷淋管道的管径损耗，通过输入多个管的一系列参数通过公式计算得出最后的结果。
	 *      </p>
	 * 
	 */
	public MainUI() {
		setName("mainUI");
		initComponents();
	}

	public static void main(String args[]) throws Exception {

//		UIManager.setLookAndFeel(UIManager.getInstalledLookAndFeels()[3].getClassName());

		new MainUI();

	}

	private void initComponents() {

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setSize(700, 500);// 大小
		setLocationRelativeTo(null);// 屏幕中间
		setResizable(false);// 禁止最大化
		setTitle(PropertiesUtil.getProperties("title"));
		Image icon = Toolkit.getDefaultToolkit().getImage( System.getProperty("user.dir")+"/sources/favicon.gif"); 
		setIconImage(icon);
		
		setLayout(new BorderLayout());
		
		try {
			gp=new GridPanel();
			tp=new TopPanel();
			rp=new ResultPanel();
		} catch (Exception e) {
			// TODO Exception Message
		}
		add(tp,BorderLayout.NORTH);
		add(gp,BorderLayout.CENTER);
		add(rp,BorderLayout.SOUTH);
		
		setVisible(true);
	}
	
	public static TopPanel tp ;
	public static GridPanel gp ;
	public static ResultPanel rp ;


	public static GridPanel getGridPanel() {
		return gp;
	}

	public static TopPanel getTopPanel() {
		return tp;
	}

	public static  ResultPanel getResultPanel() {
		return rp;
	}
	


}
