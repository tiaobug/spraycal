package com.tiaobug.spray.domain;

public class Record {

	private String id;
	private float diameter;// 管径
	private int sprinklerNum;// 喷头数目
	private float branchLen;// 管道长度
	private float flowRate;// 流量（升/秒）
	private float flowSpeed;// 流速（米/秒）
	private float lossRate;// 水损系数（KPa/m）
	private float alongWayLoss;//沿程水损（m）

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public float getDiameter() {
		return diameter;
	}

	public void setDiameter(float diameter) {
		this.diameter = diameter;
	}

	public int getSprinklerNum() {
		return sprinklerNum;
	}

	public void setSprinklerNum(int sprinklerNum) {
		this.sprinklerNum = sprinklerNum;
	}

	public float getBranchLen() {
		return branchLen;
	}

	public void setBranchLen(float branchLen) {
		this.branchLen = branchLen;
	}

	public float getFlowRate() {
		return flowRate;
	}

	public void setFlowRate(float flowRate) {
		this.flowRate = flowRate;
	}

	public float getFlowSpeed() {
		return flowSpeed;
	}

	public void setFlowSpeed(float flowSpeed) {
		this.flowSpeed = flowSpeed;
	}

	public float getLossRate() {
		return lossRate;
	}

	public void setLossRate(float lossRate) {
		this.lossRate = lossRate;
	}

	public float getAlongWayLoss() {
		return alongWayLoss;
	}

	public void setAlongWayLoss(float alongWayLoss) {
		this.alongWayLoss = alongWayLoss;
	}

	
}
