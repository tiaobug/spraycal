package com.tiaobug.spray.domain;

import javax.swing.table.DefaultTableModel;

public class GridTabelModel extends DefaultTableModel {
	private static final long serialVersionUID = -4649104674264596997L;

	public GridTabelModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GridTabelModel(int rowCount, int columnCount) {
		super(rowCount, columnCount);
		// TODO Auto-generated constructor stub
	}

	public GridTabelModel(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
		// TODO Auto-generated constructor stub
	}

	public GridTabelModel(Object[][] data, Object[] columnNames) {
		super(data, columnNames);
		// TODO Auto-generated constructor stub
	}

	public boolean isCellEditable(int row, int column) {// 不可编辑
		if (column < 4 && column > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	

}
