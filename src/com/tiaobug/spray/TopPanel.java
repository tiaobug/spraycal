package com.tiaobug.spray;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import com.tiaobug.spray.listeners.ButtonActionHandler;
import com.tiaobug.spray.listeners.FiledActionHandler;
import com.tiaobug.spray.listeners.HelpActionListener;
import com.tiaobug.spray.util.PropertiesUtil;

public class TopPanel extends JPanel {

	private static final long serialVersionUID = -847268762782661490L;

	public TopPanel() {
		super();
		intiGridPanel();
	}

	public void intiGridPanel() {
		diameter = new JComboBox(PropertiesUtil.getPropertiesArray("outsideDiameter"));
		DecimalFormat df1 = new DecimalFormat("##");
		DecimalFormat df2 = new DecimalFormat("##.00");
		sprinklerNum = new JFormattedTextField(df1);
		branchLen = new JFormattedTextField(df2);
		submitButton = new JButton(ButtonActionHandler.ADD);
		submitButton.addActionListener(new ButtonActionHandler());
		deleteButton = new JButton(ButtonActionHandler.DELETE);
		deleteButton.addActionListener(new ButtonActionHandler());
		submitButton.setBorder(BorderFactory.createBevelBorder(WHEN_FOCUSED));
		submitButton.setContentAreaFilled(false);
		deleteButton.setBorder(BorderFactory.createBevelBorder(WHEN_FOCUSED));
		deleteButton.setContentAreaFilled(false);

		JPanel buttonPane = new JPanel();
		buttonPane.setPreferredSize(new Dimension(680, 90));
		buttonPane.setLayout(new GridLayout(0,7,1, 1));
		buttonPane.setBorder(BorderFactory.createTitledBorder("分组框")); // 设置面板边框，实现分组框的效果，此句代码为关键代码

		buttonPane.setBorder(BorderFactory.createLineBorder(Color.WHITE));// 设置面板边框颜色

		JLabel diameterJLabel = new JLabel("管径：");// 管径类型
		JLabel sprinklerNumJLabel = new JLabel("喷头数目：");// 喷头数目
		JLabel branchLenJLabel = new JLabel("管道长度：");// 管道长度
		diameterJLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		sprinklerNumJLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		branchLenJLabel.setHorizontalAlignment(SwingConstants.RIGHT);

		// 添加输入框侦听时间
		sprinklerNum.addFocusListener(new FiledActionHandler());
//		branchLen.addFocusListener(new FiledActionHandler());

		buttonPane.add(diameterJLabel);
		buttonPane.add(diameter);
		buttonPane.add(sprinklerNumJLabel);
		buttonPane.add(sprinklerNum);
		buttonPane.add(branchLenJLabel);
		buttonPane.add(branchLen);
		buttonPane.add(new JLabel("米"));

		JLabel flowRateJLabel = new JLabel("流量：");// 管径类型
		JLabel flowSpeedJLabel = new JLabel("流速：");// 喷头数目
		JLabel lossRateJLabel = new JLabel("水损系数：");// 管道长度
		flowRateJLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		flowSpeedJLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lossRateJLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		
		buttonPane.add(flowRateJLabel);
		buttonPane.add(flowRate);
		buttonPane.add(flowSpeedJLabel);
		buttonPane.add(flowSpeed);
		buttonPane.add(lossRateJLabel);
		buttonPane.add(lossRate);
		
		
		buttonPane.add(new JLabel());
		buttonPane.add(new JLabel());
		buttonPane.add(new JLabel());
		buttonPane.add(new JLabel());
		buttonPane.add(new JLabel());
		buttonPane.add(new JLabel());

		
		
		buttonPane.add(submitButton);
		buttonPane.add(deleteButton);




		add(buttonPane);
	}

	private JComboBox diameter;// 管径类型
	private JTextField sprinklerNum;// 喷头数目
	private JTextField branchLen;// 管道长度
	private JButton submitButton;
	private JButton deleteButton;

	private JLabel flowRate = new JLabel();// 流量（升/秒）
	private JLabel flowSpeed = new JLabel();// 流速（米/秒）
	private JLabel lossRate = new JLabel();// 水损系数（KPa/m）
	private JLabel alongWayLoss = new JLabel();// 沿程水损（m）

	public JComboBox getDiameter() {
		return diameter;
	}

	public JTextField getSprinklerNum() {
		return sprinklerNum;
	}

	public JTextField getBranchLen() {
		return branchLen;
	}

	public JLabel getFlowRate() {
		return flowRate;
	}

	public JLabel getFlowSpeed() {
		return flowSpeed;
	}

	public JLabel getLossRate() {
		return lossRate;
	}

	public JLabel getAlongWayLoss() {
		return alongWayLoss;
	}

}
