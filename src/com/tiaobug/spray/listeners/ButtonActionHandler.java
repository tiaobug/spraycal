package com.tiaobug.spray.listeners;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import com.tiaobug.spray.GridPanel;
import com.tiaobug.spray.MainUI;
import com.tiaobug.spray.TopPanel;
import com.tiaobug.spray.domain.GridTabelModel;
import com.tiaobug.spray.domain.Record;
import com.tiaobug.spray.util.ArrayAndListHelper;
import com.tiaobug.spray.util.ExportUtil;
import com.tiaobug.spray.util.MessageUtil;
import com.tiaobug.spray.util.PropertiesUtil;

public class ButtonActionHandler implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		String buttonName = e.getActionCommand();
		if (buttonName.equals(ADD)) {
			add(e);
		} else if (buttonName.equals(DELETE)) {
			delete(e);
		} else if (buttonName.equals(CALCULATE)) {
			calculate();
		}	else if (buttonName.equals(EXPORT)) {
			calculate();
			ExportUtil.export2Txt();
		}
	}

	public void add(ActionEvent e) {
		List<Record> list = ArrayAndListHelper.getRealDataList();
		Record r = initRecord(e);
		if (r != null) {
			list.add(r);
			DefaultTableModel tableModel = new GridTabelModel(ArrayAndListHelper.List2Array(list), GridPanel.columnNames);
			MainUI.getGridPanel().getTable().setModel(tableModel);
			initTable(table);
		}
	}

	public void delete(ActionEvent e) {
		int selectRow = MainUI.getGridPanel().getTable().getSelectedRow();
		if (selectRow < 0) {
			MessageUtil.showWarn("请在表格中选择一行，然后再点删除！");
			return;
		}
		List<Record> list = ArrayAndListHelper.getRealDataList();
		list.remove(selectRow);
		DefaultTableModel tableModel = new GridTabelModel(ArrayAndListHelper.List2Array(list), GridPanel.columnNames);
		MainUI.getGridPanel().getTable().setModel(tableModel);
		initTable(table);

	}

	public void calculate() {
		List<Record> list = ArrayAndListHelper.getRealDataList();
		float calResult=0f;
		for (int i = 0; i <list.size(); i++) {
			calResult+=list.get(i).getAlongWayLoss();
		}
		DecimalFormat df = new DecimalFormat("##0.0000");
		MainUI.getResultPanel().getLossAlongWay().setText(String.valueOf(df.format(calResult))+"米");
		String rate=MainUI.getResultPanel().getLocalLossRate().getSelectedItem().toString().replaceAll("%", "");
		float allCalRestult=calResult*(1f+Float.parseFloat(rate)/100f);
		MainUI.getResultPanel().getLossAll().setText(String.valueOf(df.format(allCalRestult))+"米");
	}

	public void setvalue(Object value, int row, int cal) {
		MainUI.getGridPanel().getTable().getModel().setValueAt(value, row - 1, cal - 1);
	}

	public static final String ADD = "加入表格";
	public static final String DELETE = "删除选择行";
	public static final String CALCULATE = "计算";
	public static final String EXPORT = "导出";
	private int readElementCount;
	private int columnCount;
	private JTable table = MainUI.getGridPanel().getTable();

	public void initTable(JTable table) {
		table.setPreferredScrollableViewportSize(new Dimension(638, 280));
		table.setForeground(new Color(0, 0, 0));
		table.setFont(table.getFont().deriveFont(Font.BOLD));
		table.setSelectionBackground(Color.GRAY);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getTableHeader().setPreferredSize(new Dimension(table.getTableHeader().getWidth(), 32));// 表头高度
		table.setRowHeight(22);// 表格高度
		table.getTableHeader().setReorderingAllowed(false);// 表头不可拖动
		SetColumnWidth(table);
		makeFace(table);
		// initData();
	}

	// 自动行宽
	public void SetColumnWidth(JTable table) {
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); // 关闭列宽自动调整，当列宽超过窗口宽度时，显示水平滚动栏
		readElementCount = 0;
		columnCount = table.getColumnCount();
		JTableHeader header = table.getTableHeader();
		int rowCount = table.getRowCount();
		Enumeration<TableColumn> columns = table.getColumnModel().getColumns();
		while (columns.hasMoreElements()) {
			readElementCount++;
			TableColumn column = (TableColumn) columns.nextElement();
			int col = header.getColumnModel().getColumnIndex(column.getIdentifier());
			int width = (int) table.getTableHeader().getDefaultRenderer().getTableCellRendererComponent(table, column.getIdentifier(), false, false, -1, col).getPreferredSize().getWidth();
			for (int row = 0; row < rowCount; row++) {
				int preferedWidth = (int) table.getCellRenderer(row, col).getTableCellRendererComponent(table, table.getValueAt(row, col), false, false, row, col).getPreferredSize().getWidth();
				width = Math.max(width, preferedWidth);
			}
			header.setResizingColumn(column);
			column.setWidth(width + table.getIntercellSpacing().width + 21);
		}
	}

	public boolean hasMoreElements() {
		return readElementCount < columnCount;
	}

	public Object nextElement() {
		return table.getColumnModel().getColumn(readElementCount - 1);
	}

	// 隔行变色
	public static void makeFace(JTable table) {

		try {
			DefaultTableCellRenderer tcr = new DefaultTableCellRenderer() {
				private static final long serialVersionUID = -6758489703228509120L;

				public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
					if (row % 2 == 0)
						setBackground(Color.white); // 设置奇数行底色
					else if (row % 2 == 1)
						setBackground(new Color(206, 231, 255)); // 设置偶数行底色
					return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				}
			};
			for (int i = 0; i < table.getColumnCount(); i++) {
				table.getColumn(table.getColumnName(i)).setCellRenderer(tcr);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public Record initRecord(ActionEvent e) {
		Record record = new Record();
		TopPanel tp = (TopPanel) (((JButton) e.getSource()).getParent().getParent());
		if (tp.getSprinklerNum().getText().equals("") || tp.getBranchLen().getText().equals("")) {
			MessageUtil.showWarn("喷头数目或管道长度不能为空，请重新填写");
			return null;
		}

		// 由DN外径，换算成内径
		float innerDiameter = Float.parseFloat(PropertiesUtil.getPropertiesList("innerDiameter").get(tp.getDiameter().getSelectedIndex()));

		float diameter = Float.parseFloat(tp.getDiameter().getSelectedItem().toString().replaceAll("DN", ""));
		int sprinklerNum = Integer.parseInt(tp.getSprinklerNum().getText());
		float branchLen = Float.parseFloat(tp.getBranchLen().getText());
		record.setDiameter(diameter);
		record.setSprinklerNum(sprinklerNum);
		record.setBranchLen(branchLen);
		record.setFlowRate( intiFlowRate(record));
		record.setFlowSpeed(initFlowSpeed(record, innerDiameter));
		record.setLossRate(initLossRate(record, innerDiameter));
		record.setAlongWayLoss(initAlongWayLoss(record));

		//恢复输入框状态
//		tp.getDiameter().setSelectedIndex(0);
		tp.getSprinklerNum().setText("");
		tp.getBranchLen().setText("");
		return record;
	}

	// 流量 = 喷头数目*4/3
	public float intiFlowRate(Record record) {
		return  ((float) record.getSprinklerNum() * 4 / 3);
	}

	// 流速 = 流量/截面积
	// 截面积 = PI*内径的平方 内径由配置文件中对应得来
	public float initFlowSpeed(Record record, float innerDiameter) {
		double crossSectionalArea = Math.PI * innerDiameter * innerDiameter / 4000000f;
		return ((float) record.getSprinklerNum() * 4 / 3) / (float) crossSectionalArea / 1000f;
	}

	//水损系数= 0.0107* 流速的平方* /((内径-1mm)^1.3)
	public float initLossRate(Record record, float innerDiameter)
	{
		float lossRate=(float)(0.0107f*(float)Math.pow(record.getFlowSpeed(), 2)/(float)Math.pow((innerDiameter)/1000, 1.3f));
		return lossRate;
	}
	
	
	// 沿程水损（m）= 水损系数 * 长度 * 0.1
	public float initAlongWayLoss(Record record)
	{
		return record.getLossRate()*record.getBranchLen()*0.1f;
	}
}
