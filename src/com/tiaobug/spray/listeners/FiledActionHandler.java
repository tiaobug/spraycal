package com.tiaobug.spray.listeners;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DecimalFormat;

import javax.swing.JFormattedTextField;

import com.tiaobug.spray.TopPanel;
import com.tiaobug.spray.domain.Record;
import com.tiaobug.spray.util.MessageUtil;
import com.tiaobug.spray.util.PropertiesUtil;

public class FiledActionHandler implements FocusListener {

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void focusLost(FocusEvent e) {
		Record record = new Record();
		TopPanel tp = (TopPanel) (((JFormattedTextField) e.getSource()).getParent().getParent());
//		if (tp.getSprinklerNum().getText().equals("")||tp.getBranchLen().getText().equals("")) {
//			return;
//		}
		if (tp.getSprinklerNum().getText().equals("") ) {
			return;
		}
		// 由DN外径，换算成内径
		float innerDiameter = Float.parseFloat(PropertiesUtil.getPropertiesList("innerDiameter").get(tp.getDiameter().getSelectedIndex()));

		float diameter = Float.parseFloat(tp.getDiameter().getSelectedItem().toString().replaceAll("DN", ""));
		int sprinklerNum = Integer.parseInt(tp.getSprinklerNum().getText());
//		float branchLen = Float.parseFloat(tp.getBranchLen().getText());
		record.setDiameter(diameter);
		record.setSprinklerNum(sprinklerNum);
//		record.setBranchLen(branchLen);
		record.setFlowRate(intiFlowRate(record));
		record.setFlowSpeed(initFlowSpeed(record, innerDiameter));
		record.setLossRate(initLossRate(record, innerDiameter));
//		record.setAlongWayLoss(initAlongWayLoss(record));
		DecimalFormat df = new DecimalFormat("##0.00");
		DecimalFormat df3 = new DecimalFormat("##0.000");
		tp.getFlowRate().setText("<html><u>"+df.format(intiFlowRate(record))+" L/s</u></html>");
		tp.getFlowSpeed().setText("<html><u>"+df.format(initFlowSpeed(record, innerDiameter))+" m/s</u></html>");
		tp.getLossRate().setText("<html><u>"+df3.format(initLossRate(record, innerDiameter))+" KPa/m</u></html>");

	}

	// 流量 = 喷头数目*4/3
	public float intiFlowRate(Record record) {
		return ((float) record.getSprinklerNum() * 4 / 3);
	}

	// 流速 = 流量/截面积
	// 截面积 = PI*内径的平方 内径由配置文件中对应得来
	public float initFlowSpeed(Record record, float innerDiameter) {
		double crossSectionalArea = Math.PI * innerDiameter * innerDiameter / 4000000f;
		return ((float) record.getSprinklerNum() * 4 / 3) / (float) crossSectionalArea / 1000f;
	}

	// 水损系数= 0.0107* 流速的平方* /((内径-1mm)^1.3)
	public float initLossRate(Record record, float innerDiameter) {
		float lossRate = (float) (0.0107f * (float) Math.pow(record.getFlowSpeed(), 2) / (float) Math.pow((innerDiameter) / 1000, 1.3f));
		return lossRate;
	}

	// 沿程水损（m）= 水损系数 * 长度 * 0.1
	public float initAlongWayLoss(Record record) {
		return record.getLossRate() * record.getBranchLen() * 0.1f;
	}

}
