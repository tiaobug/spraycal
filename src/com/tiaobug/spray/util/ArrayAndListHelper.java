package com.tiaobug.spray.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.TableModel;

import com.tiaobug.spray.MainUI;
import com.tiaobug.spray.domain.Record;

public class ArrayAndListHelper {
	
	private static List<Record> realDataList=new ArrayList<Record>();
	
	
	public static List<Record> getRealDataList() {
		return realDataList;
	}
	
	public static List<Record> getAllDataAsList(TableModel tableModel) {
		List<Record> list = new ArrayList<Record>();
		int rowNum = tableModel.getRowCount();
		Record record = null;
		for (int i = 0; i < rowNum; i++) {
			record = new Record();
			record.setDiameter(Float.parseFloat(tableModel.getValueAt(i, 1).toString().replaceAll("DN", "")));
			record.setSprinklerNum(Integer.parseInt(tableModel.getValueAt(i, 2).toString()));
			record.setBranchLen(Float.parseFloat(tableModel.getValueAt(i, 3).toString()));
			record.setFlowRate(Float.parseFloat(tableModel.getValueAt(i, 4).toString()));
			record.setFlowSpeed(Float.parseFloat(tableModel.getValueAt(i, 5).toString()));
			record.setLossRate(Float.parseFloat(tableModel.getValueAt(i, 6).toString()));
			record.setAlongWayLoss(Float.parseFloat(tableModel.getValueAt(i, 7).toString()));
			list.add(record);
		}
		return list;
	}
	public static List<Record> getAllDataAsList() {
		TableModel tableModel=MainUI.getGridPanel().getTable().getModel();
		List<Record> list = new ArrayList<Record>();
		int rowNum = tableModel.getRowCount();
		Record record = null;
		for (int i = 0; i < rowNum; i++) {
			record = new Record();
			record.setDiameter(Float.parseFloat(tableModel.getValueAt(i, 1).toString().replaceAll("DN", "")));
			record.setSprinklerNum(Integer.parseInt(tableModel.getValueAt(i, 2).toString()));
			record.setBranchLen(Float.parseFloat(tableModel.getValueAt(i, 3).toString()));
			record.setFlowRate(Float.parseFloat(tableModel.getValueAt(i, 4).toString()));
			record.setFlowSpeed(Float.parseFloat(tableModel.getValueAt(i, 5).toString()));
			record.setLossRate(Float.parseFloat(tableModel.getValueAt(i, 6).toString()));
			record.setAlongWayLoss(Float.parseFloat(tableModel.getValueAt(i, 7).toString()));
			list.add(record);
		}
		return list;
	}

	
	
	
	public static Object[][] List2Array(List<Record> list) {
		Object[][] o = new Object[list.size()][8];
		DecimalFormat df = new DecimalFormat("##0.00");
		DecimalFormat df3 = new DecimalFormat("##0.000");
		for (int i = 0; i < list.size(); i++) {
			Record record = list.get(i);
			o[i][0] = i + 1;
			o[i][1] = "DN" + String.valueOf(record.getDiameter()).split("\\.")[0];
			o[i][2] = record.getSprinklerNum();
			o[i][3] = df.format(record.getBranchLen());
			o[i][4] = df.format(record.getFlowRate());
			o[i][5] = df.format(record.getFlowSpeed());
			o[i][6] = df3.format(record.getLossRate());
			o[i][7] = df.format(record.getAlongWayLoss());
		}
		return o;
	}
}
