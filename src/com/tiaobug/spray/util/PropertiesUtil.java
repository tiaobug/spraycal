package com.tiaobug.spray.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class PropertiesUtil {

	public static class PropertiesLoader {
		private PropertiesLoader() {

		}

		private static Properties props = new Properties();

		public static Properties getProp() {
			String filePath = System.getProperty("user.dir") + "/conf/data.properties";
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "utf-8"));// 中文
				props.load(br);
			} catch (Exception e) {
				// TODO Exception Message
				e.printStackTrace();
			}
			return props;
		}
	}

	// 读取properties的全部信息
	public static String getProperties(String key) {
		Properties props = PropertiesLoader.getProp();
		String property = props.getProperty(key);
		return property;
	}

	public static String[] getPropertiesArray(String key) {
		Properties props = PropertiesLoader.getProp();
		String property = props.getProperty(key);
		return property.split("#");
	}

	public static List<String> getPropertiesList(String key) {
		String[] s = getPropertiesArray(key);
		List<String> list = new ArrayList<String>();
		list.addAll(Arrays.asList(s));
		return list;
	}
	
}
