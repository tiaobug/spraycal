package com.tiaobug.spray.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import com.tiaobug.spray.GridPanel;
import com.tiaobug.spray.MainUI;
import com.tiaobug.spray.domain.Record;

public class ExportUtil {

	public static void export2Txt() {
		String partloss = MainUI.getResultPanel().getLossAlongWay().getText();
		String allLoss = MainUI.getResultPanel().getLossAll().getText();
		String lossRate = MainUI.getResultPanel().getLocalLossRate().getSelectedItem().toString();

		List<Record> list = ArrayAndListHelper.getAllDataAsList();
		String[] columnNames = GridPanel.columnNames;


		Process process=null;
		OutputStreamWriter osw;
		try {
			process=Runtime.getRuntime().exec("notepad");
			OutputStream outputStream=process.getOutputStream();
			outputStream.write("Input".getBytes());
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		try {
//
//			osw = new OutputStreamWriter(process.getOutputStream(), "UTF-8");
//			BufferedWriter bw = new BufferedWriter(osw);
//			bw.write("计算导出结果：");
//			bw.newLine();
//			StringBuffer sb = new StringBuffer();
//			for (int i = 0; i < columnNames.length; i++) {
//				sb.append(columnNames[i] + " ");
//			}
//			bw.write(sb.toString());
//			bw.newLine();
//			for (int i = 0; i < list.size(); i++) {
//				Record record = list.get(i);
//				bw.write(i + "       " + record.getDiameter() + "        " + record.getSprinklerNum() + "        " + record.getBranchLen() + "        " + record.getFlowRate() + "        " + record.getFlowSpeed() + "        " + record.getLossRate() + "        "
//						+ record.getAlongWayLoss());
//				bw.newLine();
//			}
//			bw.newLine();
//			bw.write("局部消耗比例：" + lossRate + ",  沿程水损：" + partloss + ", 总水损：" + allLoss + "   。");
//			bw.newLine();
//			bw.newLine();
//			bw.newLine();
//			bw.write("参数一：编号（auto）");
//			bw.newLine();
//			bw.write("参数二：管径（毫米）");
//			bw.newLine();
//			bw.write("  外径（DN25,DN32,DN40,DN50,DN65,DN80,DN100, DN150,DN200）");
//			bw.newLine();
//			bw.write("  内径（26.0,34.8,40.0,52.0,67.0,79.5,105.0,155.0,200.0）");
//			bw.newLine();
//			bw.write("参数三：喷头数目（int）");
//			bw.newLine();
//			bw.write("参数四：管道长度（int）(米)");
//			bw.newLine();
//			bw.write("参数五：局部损失（10% 20% 30%）");
//			bw.newLine();
//			bw.write("计算一：流量（升/秒） =  参数三*4/3（精度0.00）");
//			bw.newLine();
//			bw.write("计算二：流速（米/秒） =  流量/截面积（精度0.00）（截面积=π*内径2） ");
//			bw.newLine();
//			bw.write("计算三：水损系数（KPa/m）= 0.0107*流速^2/内径^1.3");
//			bw.newLine();
//			bw.write("计算四：沿程水损（m） =  管道长度*水损系数*0.1 ");
//
//			bw.flush();
//			bw.close();
//		} catch (IOException e) {
//			e.printStackTrace();
////			MessageUtil.showWarn("读写文件有误，请删除D盘下的export.txt文件，重新导出");
//		}
//		MessageUtil.showWarn("导出成功，文件存放在D盘下的export.txt。");


	}
}
